import React, { Component } from 'react';
import logo from './logo.svg';
import TwitterLogin from 'react-twitter-auth/lib/react-twitter-auth-component.js';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = { isAuthenticated: false, user: null, token: '' };
  }

  onSuccess = (response) => {
    const token = response.headers.get('x-auth-token');
    response.json().then(user => {
      if (token) {
        this.setState({isAuthenticated: true, user: user, token: token});
      }
    });
  };

  onFailed = (error) => {
    alert(error);
  };

  render() {
    let content = !!this.state.isAuthenticated ?
      (
        <div>
          <p>Authenticated</p>
          <div>
            {this.state.user.email}
          </div>
          <div>
            <button onClick={this.logout} className="button" >
              Log out
            </button>
          </div>
        </div>
      ) :
      (
        <TwitterLogin loginUrl="http://127.0.0.1:3001/auth/twitter"
          onFailure={this.onFailed} onSuccess={this.onSuccess}
          requestTokenUrl="http://127.0.0.1:3001/auth/twitter/reverse"/>
      );

    return (
      <div className="App">
        {content}
      </div>
    );
  }

  logout = () => {
    this.setState({isAuthenticated: false, token: '', user: null})
  };
}

export default App;
